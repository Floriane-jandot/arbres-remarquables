package fr.jandot.arbres_remarquables_jandot_floriane_g2s4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;

public class AdapterArbres extends BaseAdapter {
    private ArrayList<Arbres> listeArbres;
    private Context context;

    public AdapterArbres(Context context, ArrayList<Arbres> listeArbres) { //Reprise du code du Tp4 :)
        this.context = context;
        this.listeArbres = listeArbres;
    }

    public ArrayList<Arbres> getListeArbres() {
        return listeArbres;
    }

    @Override
    public int getCount() {
        return listeArbres.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ConstraintLayout layoutItem;
        LayoutInflater mInflater = LayoutInflater.from(context);
        //(1) : Réutilisation du layout
        if (convertView == null) {
            layoutItem = (ConstraintLayout) mInflater.inflate(R.layout.item_layout, parent, false);
        } else {
            layoutItem = (ConstraintLayout) convertView;
        }

        //(2) : Récupération des TextView de notre layout
        TextView tv = (TextView)layoutItem.findViewById(R.id.textView);
        ImageView imageView = layoutItem.findViewById(R.id.imageView);

        //(3) : Mise à jour des valeurs
        Arbres arbre = listeArbres.get(position);
        tv.setText(arbre.getLibellefrancais());

        //On retourne l'item créé.
        return layoutItem;
    }
}
