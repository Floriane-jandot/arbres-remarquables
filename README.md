Petite application android créée en 2021.

À partir d'une base de donnée des arbres remarquables de France, l'application affiche une liste déroulante contenant le nom de chacun des arbres. 
L'utilisateur peut cliquer sur un élément pour obtenir toutes les informations de l'arbre ainsi que sa position géographique sur une carte Google Maps.
