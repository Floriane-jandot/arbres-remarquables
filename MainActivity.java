package fr.jandot.arbres_remarquables_jandot_floriane_g2s4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    ArrayList<Arbres> listeArbres = new ArrayList<>();
    private String URL = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=arbresremarquablesparis2011&q=&rows=20&facet=genre&facet=espece&facet=stadedeveloppement&facet=varieteoucultivar&facet=dateplantation&facet=libellefrancais";
    AdapterArbres adapter = new AdapterArbres(MainActivity.this, listeArbres);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listview = findViewById(R.id.listView);
        listview.setAdapter(adapter); //chargement de la vue
        new Connexion_flux().execute(URL, listeArbres, adapter); //Accès aux données

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(MainActivity.this, Information_arbre.class);
                intent.putExtra("lib", listeArbres.get(position).getLibellefrancais()); //envoi les données dans la classe d'affichage des arbres
                intent.putExtra("esp", listeArbres.get(position).getEspece()); //j'aurais directement pu faire passer l'objet mais j'ai préféré faire comme ça
                intent.putExtra("gen", listeArbres.get(position).getGenre());
                intent.putExtra("h", listeArbres.get(position).getHauteurenm());
                intent.putExtra("adr", listeArbres.get(position).getAdresse());
                startActivity(intent);
            }

        });

    }

    public void onClickMap(View view){ //Emmène sur la carte
        Intent intent = new Intent(MainActivity.this, MapsActivity.class);
        intent.putExtra("liste",adapter.getListeArbres()); //La liste du main était vide, donc je l'ai fait venir de mon adapter
        startActivity(intent);
    }


}