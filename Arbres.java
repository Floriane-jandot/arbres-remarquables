package fr.jandot.arbres_remarquables_jandot_floriane_g2s4;

import java.io.Serializable;

public class Arbres implements Serializable {

    private String libellefrancais;
    private String espece;
    private String genre;
    private String hauteurenm;
    private String adresse;
    private double x;
    private double y;

    public Arbres(String libellefrancais, String espece, String genre, String hauteurenm, String adresse, double x, double y) {
        this.libellefrancais = libellefrancais;
        this.espece = espece;
        this.genre = genre;
        this.hauteurenm = hauteurenm;
        this.adresse = adresse;
        this.x = x;
        this.y = y;
    }

    public String getEspece() {
        return espece;
    }

    public String getGenre() {
        return genre;
    }

    public String getHauteurenm() {
        return hauteurenm;
    }

    public String getAdresse() {
        return adresse;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getLibellefrancais() {
        return libellefrancais;
    }
}


