package fr.jandot.arbres_remarquables_jandot_floriane_g2s4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Information_arbre extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information_arbres);

        Intent intent = getIntent();

        TextView textLibelle = (TextView)findViewById(R.id.libellefr); //On va chercher les TextView du layout pour les remplir
        TextView textGenre = (TextView)findViewById(R.id.genre);
        TextView textEspece = (TextView)findViewById(R.id.espece);
        TextView textHauteur = (TextView)findViewById(R.id.taille);
        TextView textAdresse = (TextView)findViewById(R.id.adresse);

        String libellefrancais = intent.getStringExtra("lib"); //contenu
        String espece = intent.getStringExtra("esp");
        String genre = intent.getStringExtra("gen");
        String hauteurenm = intent.getStringExtra("h");
        String adresse = intent.getStringExtra("adr");

        textLibelle.setText("Nom: " + libellefrancais); //remplissage
        textGenre.setText("Genre: " + genre);
        textEspece.setText("Espèce: " + espece);
        textHauteur.setText("Taille (en m): " + hauteurenm);
        textAdresse.setText("Adresse: " + adresse);


    }
    public void onClick(View view){ //Retour sur la listView
        Intent intent = new Intent(view.getContext(), MainActivity.class);
        startActivityForResult(intent, 0);
    }
}
