package fr.jandot.arbres_remarquables_jandot_floriane_g2s4;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        ArrayList<Arbres> listeArbres = (ArrayList<Arbres>) getIntent().getSerializableExtra("liste");
        for (int i=0; i<listeArbres.size(); i++){
            LatLng position = new LatLng(listeArbres.get(i).getX(),listeArbres.get(i).getY()); //Position des arbres
            MarkerOptions mOptions = new MarkerOptions().position(position).title(listeArbres.get(i).getLibellefrancais()); //indication de l'espèce
            mMap.addMarker(mOptions);
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(48.864716, 2.349014))); //Centre de Paris
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 10.0f ) ); //zoom
    }
}