package fr.jandot.arbres_remarquables_jandot_floriane_g2s4;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class Connexion_flux extends AsyncTask<Object, Void, String> {

    private String URL;
    private ArrayList<Arbres> listeArbres;
    private AdapterArbres adapter;

    @Override
    protected String doInBackground(Object... params) {

        URL = (String) params[0];
        listeArbres = (ArrayList<Arbres>) params[1];
        adapter = (AdapterArbres) params[2];

        URL url=null;
        HttpURLConnection urlConnection = null;
        String flux ="";
        String temp ="";
        try {
            url = new URL(URL);
            urlConnection = (HttpsURLConnection) url.openConnection();
            if (urlConnection.getResponseCode() == HttpsURLConnection.HTTP_OK) {
                // chargement du flux
                InputStreamReader isr = new InputStreamReader(urlConnection.getInputStream());
                BufferedReader input = new BufferedReader(isr);
                StringBuilder stringBuilder = new StringBuilder();
                while ((temp = input.readLine()) != null) {
                    stringBuilder.append(temp);
                }
                String jsonStr = stringBuilder.toString();

                // parsage du flux
                JSONObject jsonRoot = new JSONObject(jsonStr);
                JSONArray jArray = jsonRoot.getJSONArray("records");

                for (int i=0; i<jArray.length(); i++){
                    JSONObject element = jArray.getJSONObject(i); //extraction des données
                    JSONObject field = element.getJSONObject("fields");
                    String libellefrancais = field.getString("libellefrancais");
                    String espece = field.getString("espece");
                    String genre = field.getString("genre");
                    String hauteurenm = field.getString("hauteurenm");
                    String adresse = field.getString("adresse");
                    JSONArray geom_x_y = field.getJSONArray("geom_x_y");
                    double x = (double) geom_x_y.get(0); //pour la géolocalisation de l'arbre
                    double y = (double) geom_x_y.get(1);
                    listeArbres.add(new Arbres(libellefrancais,espece,genre,hauteurenm,adresse,x,y));
                }

                input.close();
            }
        } catch (IOException | JSONException ioe) {
            ioe.printStackTrace();
            Log.d("AsyncTask", "pb connexion");
            String text = "pb connexion";
        /*
        } catch (JSONException e) { //Je ne sais pas pourquoi il y a une erreur...
            e.printStackTrace();
            String text = "erreur de format de donnée";

        */
        } finally {
            urlConnection.disconnect(); // on passe systematiquement par là
        }
        return null;
    }
    @Override
    protected void onPostExecute(String result) { //Rafraîchissement
        adapter.notifyDataSetChanged();
    }
}
